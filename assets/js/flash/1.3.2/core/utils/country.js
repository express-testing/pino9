/**
 * v 1.0
 * JS User Country Detector
 * ------------------------
 */

flashCore.prototype.getUserCountry = function(callback) {
	if(readCookie('user_country')) return callback(readCookie('user_country'));

	var request = new XMLHttpRequest();
	request.open('GET', 'http://ip-api.com/json/?callback=?', true);

	request.onload = function() {
	  if (request.status >= 200 && request.status < 400) {
	    // Success!
	    var data = JSON.parse(request.responseText);
	    createCookie('user_country', data.countryCode);
        return callback(data.countryCode);
	  } else {
	    // We reached our target server, but it returned an error

	  }
	};

	request.onerror = function() {
	  // There was a connection error of some sort
	};

	request.send();
}

// Usage:
// ======
// flash.getUserCountry(function(country) {
// 	console.log('user country: ' + country);
// });